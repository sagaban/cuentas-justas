import { HelloWorld } from "../components";

export default {
  title: "Components/HelloWorld",
  component: HelloWorld,
  argTypes: {}
};

//👇 We create a “template” of how args map to rendering
const Template = (args) => ({
  components: { HelloWorld },
  setup() {
    return { args };
  },
  template: '<HelloWorld v-bind="args" />',
});

export const Default = Template.bind({});
Default.args = {
  msg: "Hola",
};
