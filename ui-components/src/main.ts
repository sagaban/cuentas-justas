import { App, Component } from "vue";
// import components from "./components";
import * as components from "./components";

const ComponentLibrary = {} as { install: (app: App) => void };

interface ComponentsObject {
  [key: string]: Component
}

const getKeyValue = <T extends object, U extends keyof T>(key: U) => (obj: T) =>
  obj[key];

ComponentLibrary.install = (app: App): void => {
  for (const componentName in components) {
    const component = getKeyValue<ComponentsObject, any>(componentName)(components)

    app.component(component.name as string, component);
  }
};

export default ComponentLibrary;
