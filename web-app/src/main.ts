import { createApp } from 'vue'
import App from './App.vue'
import UILibrary from '../../ui-components/';

export const app = createApp(App)

app.use(UILibrary)

app.mount('#app');
